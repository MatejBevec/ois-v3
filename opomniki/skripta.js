window.addEventListener("load", function() {
	// Stran naložena
	
	var prijavniGumb = document.getElementById("prijavniGumb");
	
	prijavniGumb.addEventListener('click', function() {
		var ime = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
	});
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas > 0){
				cas--;
				casovnik.innerHTML = cas;
			}else{
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev " + naziv_opomnika + " je potekla!");
				opomnik.parentElement.removeChild(opomnik);
			}
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
	//dodaj opomnik
	function dodajOpomnik(){
		var nazivPolje = document.getElementById("naziv_opomnika");
		var casPolje = document.getElementById("cas_opomnika");
		
		//pridobivanje naziva in casa
		var naziv = nazivPolje.value;
		var cas = casPolje.value;
		nazivPolje.value = "";
		casPolje.value = "";
		
		//dodajanje opomnika
		if (naziv != "" && cas != ""){
			document.getElementById("opomniki").innerHTML += " \
				<div class='opomnik senca rob'> \
					<div class='naziv_opomnika'>" + naziv + "</div> \
					<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div> \
				</div> \
			";
			}
		}
		
		
		document.getElementById("dodajGumb").addEventListener("click", dodajOpomnik);
	
});


